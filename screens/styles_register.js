const React = require("react-native");

const { StyleSheet } = React;

export default {

containerView: {
  flex: 1,
},
registerScreenContainer: {
  flex: 1,
},
logoText: {
  fontSize: 40,
  fontWeight: "800",
  marginTop: 150,
  marginBottom: 30,
  textAlign: 'center',
},
registerFormView: {
  flex: 1
},
registerFormTextInput: {
  height: 43,
  fontSize: 14,
  borderRadius: 5,
  borderWidth: 1,
  borderColor: '#eaeaea',
  backgroundColor: '#fafafa',
  paddingLeft: 10,
  marginLeft: 15,
  marginRight: 15,
  marginTop: 5,
  marginBottom: 5,

},
RegisterButton: {
  backgroundColor: '#a31a1a',
  borderRadius: 5,
  height: 45,
  marginTop: 10,
}
};
