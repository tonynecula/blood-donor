import React, { Component } from "react";
import styles_register from "./styles_register";
import {
  Keyboard, Text, View, TextInput, StyleSheet, Platform,
  TouchableWithoutFeedback, Alert, KeyboardAvoidingView, Image
} from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';
//import { onSignIn } from './../appState/appStore';
import firebase from './../utils/firebase';
import * as Facebook from 'expo-facebook'
import * as Progress from 'react-native-progress';
import ProgressBar from 'react-native-progress/Bar';
export default class RegisterScreen extends Component {

  constructor(props) {
    super(props);

    console.log('RegisterScreen constructor');

    this.state = {
      isLoading: false
    };
  }

  signInWithFacebook = async () => {
    this.setState({isLoading: true});
    
    const appId = Expo.Constants.manifest.extra.facebook.appId;
    const permissions = ['public_profile', 'email'];  // Permissions required, consult Facebook docs
    await Facebook.initializeAsync(appId);
    const fbRez = await Facebook.logInWithReadPermissionsAsync(
       {
        permissions: permissions,
        //behavior: 'web'
      }
    );
    
  
    if (fbRez.type == "cancel") {
      this.setState({isLoading: false});
      return Promise.reject({ type: 'cancel' });
    }

    switch (fbRez.type) {
      case 'success': {
        await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);  // Set persistent auth state
        const credential = firebase.auth.FacebookAuthProvider.credential(fbRez.token);
        const userData = await firebase.auth().signInWithCredential(credential);
        console.log('user data:', userData);
        this.setState({isLoading: false});
        // Do something with Facebook profile data
        // OR you have subscribed to auth state change, authStateChange handler will process the profile data
        return Promise.resolve({ type: 'success', userData: userData });
      }
      case 'cancel': {
        return Promise.reject({ type: 'cancel' });
      }
    }
  }

  registerWithFacebook() {
    //TODO: implement FB registration
    //register ... https://medium.com/@juliofeferman/running-facebook-authentication-in-react-native-with-firebase-219a002588fa
    console.log(this.props);
    this.props.isUserLoggedIn = true;
    this.signInWithFacebook().then((loginResult) => {
      if (loginResult.type == 'success') {
        this.props.navigation.navigate("Main");       
      }
    });
  }

  componentDidMount () {  }
 
  renderButon(){
    if(!this.state.isLoading)
      return (
        <ThemeProvider theme={theme}>
          
                      <Button
                        buttonStyle={styles.loginButtonn}
                        onPress={() => {
                          this.registerWithFacebook();
                        }}
                        title="Continue with Facebook"
                      />
                      </ThemeProvider> 
           
        
      );
      else return (<View></View>);
  }

  render() {
    console.log('render register  -> renderComponent');
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView style={styles_register.containerView} behavior="padding">
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles_register.registerScreenContainer}>
              <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 140, justifyContent: 'center', alignItems: 'center'}}>
                  <Image source={require('./../assets/images/bloody.png')}
                    style={styles.logo} />
                    <Text style={{fontWeight: '200', fontSize: 17, alignSelf: 'center'}}>
                      An app created for blood donors 
                    </Text>
              </View>
              <View style={styles_register.registerFormView}>
                <View style={styles.bottom}>
                  {this.state.isLoading && 
                      <ThemeProvider theme={theme}>
                      <Button
                        buttonStyle={styles.loginButtonn}
                        title="Loading ... please wait"
                      />
                      </ThemeProvider>                  
                  }
                  {this.renderButon()}
                  
             </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const theme = {
  Button: {
    titleStyle: {
      color: 'white',
    },
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  logo: {
    width: 300,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',

  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 36
  }
  ,
  loginButtonn: {
    backgroundColor: "#BA272A",
    borderRadius: 30,
    height: 54,
    marginTop: 30,
    width: 300,
    justifyContent: "center",
    alignSelf: "center"
  },
  });
