import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image,TouchableOpacity,AppRegistry } from 'react-native';
import DropDownItem from 'react-native-drop-down-item';
import { ExpoLinksView } from '@expo/samples';

import { Linking } from 'react-native';
const IC_ARR_DOWN = require('./../assets/images/icons/ic_arr_down.png');
const IC_ARR_UP = require('./../assets/images/icons/ic_arr_up.png');

export default class InfoScreen extends Component {
  state = {
    contents: [
      {
        title: 'Cine are nevoie de sânge?',
        body: <Text style={{fontWeight:'400'}}>Cea mai mare nevoie este la pacienții care au nevoie de operații, de transplant, care au suferit arsuri sau au fost diagnosticați cu cancer. De asemenea, victimele accidentelor și copiii nou-născuți care au diferite anemii, au nevoie de transfuzii sanguine pentru a supraviețui.</Text>
      },
      {
        title: 'De ce este nevoie de sânge?',
        body: <Text style={{fontWeight:'400'}}>Sângele nu se poate produce sau cumpăra. El poate fi obținut doar prin donare.</Text>
      },
      
      {
        title: 'Cine poate dona?',
        body: 
          
            <Text style={{fontWeight:'400'}}>
              Condiții pentru a deveni donator de sange:
              {'\n'}{'\n'}
                  - să ai vârsta între 18-60 ani{'\n'}
                  - să ai greutate peste 50 kg{'\n'}
                  - să ai un puls regulat, 60 -100 bătăi/minut{'\n'}
                  - să ai tensiunea arteriala sistolică între 100 si 180 mmHg{'\n'}
                  - să nu fi suferit in ultimele 6 luni intervenții chirurgicale{'\n'}
                  - femeile să nu fie: însarcinate, în perioada de lauzie, in perioada menstruală{'\n'}
                  - să nu fi consumat grăsimi sau băuturi alcoolice cu cel puțin 48 de ore înaintea donării{'\n'}
                  - să nu fii sub tratament pentru afecțiuni ca: hipertensiune, boli de inimă, boli renale, boli psihice, boli hepatice, boli endocrine{'\n'}
                  - să nu ai sau să nu fi avut hepatita (de orice tip), sifilis, malarie, epilepsie si alte boli neurologice, boli psihice, bruceloză, ulcer, diabet zaharat, boli de inimă, boli de piele: psoriazis, vitiligo, miopie peste (-) 6 dioptrie, cancer


            </Text>
       },
      {
        title: 'Cât de des pot dona?',
        body: <Text style={{fontWeight:'400'}}>Frecvenţa donărilor de sânge total nu poate depăşi: 5 donări pe an pentru bărbaţi şi 4 donări pe an pentru femei.{'\n'}
        Intervalul între două donări este de cel puţin 8 săptămâni.</Text>,
      },
      {
        title: 'Unde pot dona?',
        body: 
          <Text>
            Momentan din locatia in care te aflii, cele mai apropriate centre sunt:{'\n'}
            <Text style={{color: '#8902ab',fontWeight:'bold'}}onPress={() => Linking.openURL('http://www.crtscluj.ro')}>
            -CTS Cluj, Str. N. Bălcescu nr.18, Telefon: 0364.411.254 / 0364.411.255;{'\n'}</Text>
            <Text style={{color: '#4824db',fontWeight:'bold'}}onPress={() => Linking.openURL('https://web.facebook.com/pages/Centrul-De-Transfuzie-Alba-Iulia/1495643830739034?_rdc=1&_rdr')}>
            -CTS Alba, B-dul Revoluţiei 1989 nr.23, Telefon: 0358.401.874T: 0358.401.632;{'\n'}</Text>
            <Text style={{color: '#BA272A',fontWeight:'bold'}}onPress={() => Linking.openURL('https://web.facebook.com/pages/Centrul-De-Transfuzie-Sanguina-Zalau-Salaj/285209125294937?_rdc=1&_rdr')}>
            -CTS Zalău,	Str. Simion Bărnuţiu nr.91,	Telefon: 0360.401.773 / 0360.401.774</Text>
        </Text>,
       
  
      },
      {
        title: 'Cum se desfasoara donarea?',
        body: 
          <Text style={{fontWeight:'400'}}>
            Etapele donării:{'\n'}
                - completarea chestionarului de către donator;{'\n'}
                - întocmirea fișei donatorului;{'\n'}
                - examenul medical al donatorului;{'\n'}
                - măsurarea tensiunii arteriale, a pulsului;{'\n'}
                - controlul biologic înainte de donare (grupă sanguină, hemoglobină, glicemie){'\n'}
                - recoltarea de sânge;{'\n'}
                - repaus post-donare obligatoriu timp de 10 minute și compresie la locul puncției.
          </Text>
      },
      {
        title: 'Cum este testat sângele meu?',
        body: 
          <Text style={{fontWeight:'400'}}>
            Testle biologice efectuate sistematic la fiecare donare cuprind:{'\n'}
            {'\n'}
            - determinarea grupei sanguine OAB si RH-ului;{'\n'}
            - determinarea hemoglobinei;{'\n'}
            - determinarea sifilisului;{'\n'}
            - determinarea anticorpilor antiHIV si antiHTlv;{'\n'}
            - determinarea hepatitei virale B si C;{'\n'}
            - determinarea enzimelor hepatice;{'\n'}
          </Text>,
      },
      {
        title: 'Ce riscuri exista?',
        body: 
          <Text style={{fontWeight:'400'}}>
            Donarea de sânge este sigură (sunt folosite doar materiale de unică folosință), iar dacă medicul te acceptă la donare, inseamnă că nu există nici un risc pentru tine din punct de vedere al sănătății.
          </Text>
      },
      {
        title: 'Ce trebuie să fac înainte și după donare?',
        body: 
          <Text style={{fontWeight:'400'}} >
          Înainte să vii la donare e important să fii odihnit, bine hidratat și să te bucuri de un mic dejun sănătos (fara multe grasimi). Încearcă să eviți alimentele grase, tutunul și băuturile alcoolice.{'\n'}
          După donare e important să te rehidratezi și să nu faci efort fizic mare în următoarele ore. În rest, nu există nici o restricție.
          </Text>
      },
      {
        title: 'Vrei ghidul complet?',
        body: 
        <Text style={{color: '#8902ab',fontWeight:'bold'}}
      onPress={() => Linking.openURL('https://www.spitaljbm.ro/images/Ghidul%20donatorului%20de%20sange.pdf')}>
  Apasă pe text pentru ghidul complet.
</Text>
      },
    ],
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ alignSelf: 'stretch' }}>
          {
            this.state.contents
              ? this.state.contents.map((param, i) => {
                return (
                  <DropDownItem
                    key={i}
                    style={styles.dropDownItem}
                    contentVisible={false}
                    invisibleImage={IC_ARR_DOWN}
                    visibleImage={IC_ARR_UP}
                    header={
                      <View style={styles.header}>
                        <Text style={{
                          fontSize: 24,
                          color: '#BA272A',
                          fontWeight: 'bold'
                        }}>{param.title}</Text>
                      </View>
                    }
                  >
                    <Text style={[
                      styles.txt,
                      {
                        fontSize: 20,
                       
                      },
                    ]}>
                      {param.body}
                    </Text>
                  </DropDownItem>
                );
              })
              : null
          }
          <View style={{ height: 96 }}/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dropDownItem:{
    // invisibleImage={position: 'absolute', top: 25,right: 0},
    // visibleImage={position: 'absolute', top: 25,right: 0}
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 10,
  },
  header: {
    width: '100%',
    paddingVertical: 8,
    paddingHorizontal: 12,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 12,
    color: 'red',
    marginRight: 60,
    flexWrap: 'wrap',
  },
  txt: {
    fontSize: 14,
  },
});

AppRegistry.registerComponent('AndroidFonts', () => AndroidFonts);
InfoScreen.navigationOptions = {
  headerTitleStyle: { alignSelf: 'center' },
  title: 'Info: tot ce trebuie să știi.',
  headerStyle: {
    backgroundColor: '#BA272A',
    textAlign:'center',
   
  },
  
  headerTintColor: 'white',
  headerTitleStyle: {
    textAlign: 'center',
    alignSelf : 'center',
    fontWeight: 'bold',
    
  },
};

