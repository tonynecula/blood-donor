import React, { Component } from "react";

import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Image,
    Dimensions,
    Button
} from "react-native";
import Product from "../components/Product";
import firebase from './../utils/firebase';
import { Actions } from 'react-native-router-flux';


class OfferScreen extends Component {
  constructor(props) {
    super(props);
    console.log('OfferScreen', props);
    console.log(props.navigation);
    console.log('Offers constructor', props);
    this.state = {
      offers: []
    };

    firebase.firestore().collection('Offers').get()
      .then(querySnapshot => {
        var newOffers = [];
        querySnapshot.docs.map(doc => {
          console.log('LOG 1', doc.data());
          var it = doc.data();
          it.id = doc.id;
          newOffers.push(it);
        });
        this.setState({ offers: newOffers });
      });
  }
  static navigationOptions =({navigation}) =>({
    title: "Oferte",
    headerTitleStyle: { alignSelf: 'center', },
    headerStyle: {
      backgroundColor: '#BA272A',
    },
    headerRight: (
    
     <View style={{paddingRight:11}}>
       
       <Button
        color="#ff5c5c" title="Tombola"
        onPress={() =>
          navigation.navigate('Post')}
       />
  
    </View> 
    ),
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  })
  
  render() {
    return (
      <View>
        {/* <View style={styles.header}></View> */}
        <ScrollView scrollEventThrottle={16}>
          <View style={[styles.body,{flex:1,backgroundColor:'white',paddingTop : '9%',paddingLeft :  20,paddingRight : 20}]}>
            {
          
              buttonsListArr = this.state.offers.map(item => (
                <Product navigation={this.props.navigation}
                  productObj={item} imageUri={item.ImageUrl} key={item.id} decription={item.Description} name={item.Title}
                  value={item.Value} />
                  
              )
              )
            }
          </View>
 
        </ScrollView>
      </View>
    );
  }
}
export default OfferScreen;

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#BA272A",
    fontWeight: 'bold',
    height:200,
  },
  body:{
    
  
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingTop: 40
  }
});