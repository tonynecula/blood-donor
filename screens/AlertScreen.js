import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from 'react-native'
import { Button, ThemeProvider } from 'react-native-elements';

export default class AlertScreen extends Component {
    constructor(props) {
      super(props);
      this.state = {
        titleText: 'Toate alertele vor apărea aici ',
        bodyText: 'Momentan nu este nici o alerta in județul tău \nFii pe fază!'
      };
    }
  
    render() {
      return (
        <Text style={styles.baseText}>
          <Text style={styles.titleText} onPress={this.onPressTitle}>
            {this.state.titleText}{'\n'}{'\n'}
          </Text>
          <Text numberOfLines={5}>
            {this.state.bodyText}
          </Text>
        </Text>
      );
    }
  }
  
  const styles = StyleSheet.create({
    baseText: {
           
            fontSize: 20,
            textAlign : 'center',
            color: '#BA272A',
            marginTop: 18,
            fontWeight: 'bold',
      
    },
    titleText: {
        
        color: 'black',
      fontSize: 25,
      fontWeight: 'bold',
      textAlign : 'center',
    },
  });
  

AlertScreen.navigationOptions = {
  headerTitleStyle: { alignSelf: 'center' },
  title: 'Alerte',
  headerStyle: {
    backgroundColor: '#BA272A',
  },

  headerTintColor: 'white',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};
