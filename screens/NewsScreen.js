import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,ImageURISource, ScrollView, Image,TouchableOpacity,AppRegistry } from 'react-native';
import DropDownItem from 'react-native-drop-down-item';
import { ExpoLinksView } from '@expo/samples';

import { Linking } from 'react-native';
const IC_ARR_DOWN = require('./../assets/images/icons/ic_arr_down.png');
const IC_ARR_UP = require('./../assets/images/icons/ic_arr_up.png');

export default class NewsScreen extends Component {
  state = {
    contents: [
      {
        image: 'https://img.digitalag.ro/?u=https%3A%2F%2Fa1.ro%2Fuploads%2Fmodules%2Fnews%2F0%2F2019%2F3%2F7%2F848452%2F1551952460a03c2fc9.jpg&w=800',
        title: 'Dolj: Peste 50 de angajaţi ISU au participat la campania de donare de sânge iniţiată de CRTS Craiova ...',
        body: 'Peste 50 de angajaţi ai Inspectoratului pentru Situaţii de Urgenţă (ISU) "Oltenia" al judeţului Dolj au participat, marţi, la campania de donare de sânge iniţiată de Centrul Regional de Transfuzie Sanguină (CRTS) Craiova ca urmare a cererilor tot mai mari de sânge din spitalele de municipiu.',
      },
      {
        image: 'https://dej24.ro/wp-content/uploads/2019/10/campanie-de-donare-de-sange.jpg',
        title: 'Stocuri de sânge epuizate în Vaslui ...',
        body: 'Spitalul Județean de Urgență Vaslui a lansat, joi, un apel public, după ce stocurile de sânge s-au epuizat. Pentru a ușura deplasarea donatorilor la Centrul de Transfuzii din Bârlad, conducerea spitalului a anunțat că pune la dispoziție mijloace de transport.',
      },
      {
        image: 'http://static.cdn.jurnaltv.md/superdesk/20191016141048/e1d15c99-7f50-45f1-a683-932f3a27ce91.jpg',
        title: 'Acţiune de donare sânge la Mangalia. Când va avea loc...',
        body: 'Centrul Regional de Transfuzii Sanguine Constanța și Primăria Năvodari organizează joi, 24 octombrie, între orele 08:30-13:30, o acțiune de donare de sânge, la Centrul de Zi pentru Copii și Persoane în Vârstă din Năvodari (lângă Serviciul Taxe și Impozite), din Strada Sănătății, nr. 2.',
      },
      {
        image: 'https://s.iw.ro/gateway/g/ZmlsZVNvdXJjZT1odHRwJTNBJTJGJTJG/c3RvcmFnZTA3dHJhbnNjb2Rlci5yY3Mt/cmRzLnJvJTJGc3RvcmFnZSUyRjIwMTcl/MkYwOCUyRjA5JTJGODA3OTM2XzgwNzkz/Nl9kb25hcmUtc2FuZ2UtZ2V0dHktZG9u/YXRvci5wbmcmdz03ODAmaD00NDAmaGFz/aD1hNTBjNmUyOTZhYzc0MWRmMDVmZDIyODIyOTg1MzBkYw==.thumb.jpg',
        title: '„Arată că îți pasă!”: Campania de donare voluntară de sânge în rândurile tineretului în plină desfășurare...',
        body: 'Mesajul evenimentului este de a îndemna tineretul, să doneze voluntar sânge, contribuind, astfel, la dăruirea de viaţă printr-un  act de spirit civic.\nCampania este organizată în contextul realizării Planului de acţiuni în cadrul Programului naţional „Securitatea transfuzională, autosigurarea ţării cu produse sanguine pentru anii 2017-2021.',
      },
      {
        image: 'https://media.cancan.ro/unsafe/720x0/smart/filters:quality(80):format(jpg)/wp-content/uploads/2019/10/campanie-donare-sange-preot.jpg',
        title: 'Un preot din Caracal a mobilizat 200 de persoane să doneze sânge...',
        body: 'Preotul Ion Iagăru , din Caracal, a reușit să mobilizeze 200 de persoane, într-o campanie de donare de sânge. La apelul făcut de preotul din Caracal au răspuns oameni simpli, dar și cadre medicale, militari și cadre didactice din Caracal și împrejurimi.Celor care nu au reușit să ajungă de această dată să doneze, preotul le-a spus că va fi organizată în luna noiembrie o nouă campanie de donare.',
      },
      
    ],
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ alignSelf: 'stretch' }}>
          {
            this.state.contents
              ? this.state.contents.map((param, i) => {
                return (
                  <DropDownItem
                    key={i}
                    style={styles.dropDownItem}
                    contentVisible={false}
                    
                    // invisibleImage={IC_ARR_DOWN}
                    // visibleImage={IC_ARR_UP}
                    header={
                      <View style={styles.header}>
                         <Image
                                  style={{width: '100%', height:150,
                                  borderColor: 'black',
                                  // fontWeight: 'bold',
                                  borderWidth: 1,
                                  borderRadius: 5,}}
                                  source={{uri: param.image }}
                                  />
                        <Text style={{
                          fontSize: 23,
                          color: '#BA272A',
                          fontWeight: 'bold'
                        }}>{param.title}</Text>
                        
                      </View>
                    }
                  >
                    <Text style={[
                      styles.txt,
                      {
                        fontSize: 19,
                        
                      },
                    ]}>
                      {param.body}
                    </Text>
                  </DropDownItem>
                );
              })
              : null
          }
          <View style={{ height: 96 }}/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 10,
  },
  header: {
    textAlign:'center',
    width: '100%',
    paddingVertical: 8,
    paddingHorizontal: 12,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 12,
    color: 'red',
    marginRight: 60,
    flexWrap: 'wrap',
    textAlign:'center',
  },
  txt: {
    fontSize: 14,
  },
});


NewsScreen.navigationOptions = {
  headerTitleStyle: { alignSelf: 'center',fontSize:30 },
    title: 'Newsfeed',
    headerStyle: {
      backgroundColor: '#BA272A',
     
    },
    
    headerTintColor: 'white',
    headerTitleStyle: {
      textAlign: 'center',
      alignSelf : 'center',
      fontWeight: 'bold',
      
    },
};
