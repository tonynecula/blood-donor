
import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  AsyncStorage,
  View,
  ScrollView,
  SafeAreaView,
} from 'react-native';

// import { AppRegistry } from 'react-native';
// import { Provider as PaperProvider } from 'react-native-paper';

// import { IconButton, Colors } from 'react-native-paper';
// import Icon from '@mdi/react';
// import { mdiBellRing } from '@mdi/js';
// import { NavigationEvents } from 'react-navigation';
import BellIcon from 'react-bell-icon';
import { Button } from 'react-native-elements';
import { onSignOut } from './../appState/appStore';
import firebase from './../utils/firebase';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';



export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalDuration: 0,
      product: null,
      currentUser: {},
      date: null
    };
    console.log('HomeScreen constructor',this.props.navigation);
    var didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        console.log('didFocus', payload);
        this.readOfferAsync();
      }
    );
  }
  componentDidMount() {
    var that = this;

    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        console.log('user: ', user);
        that.setState({ currentUser: user });
      }
    });

    this.readOfferAsync();


    var date = moment()
      .utcOffset('+05:30')
      .format('YYYY-MM-DD hh:mm:ss');


    var expirydate = '2019-10-26 04:00:45';
    var diffr = moment.duration(moment(expirydate).diff(moment(date)));

    var hours = parseInt(diffr.asHours());
    var minutes = parseInt(diffr.minutes());
    var seconds = parseInt(diffr.seconds());

    var d = hours * 60 * 60 + minutes * 60 + seconds;
    that.setState({ totalDuration: d });
  }

  readOfferAsync = async () => {
    try {
      console.log('product read');
      let item = await AsyncStorage.getItem('product');
      console.log('product read', item);
      if (item) {
        let product = JSON.parse(item);
        console.log('product read', item);
        console.log('product timestamp', product.timestamp);
        let startDate = moment.utc(product.timestamp);
        console.log("promotion start: " +  startDate.format());
        let expirationDate = startDate.add(60, 'days');
        console.log("promotion end:  " +  expirationDate.format());
        let secondsLeft = expirationDate.diff(moment.utc(), "seconds");
        this.setState({ product: product, date: secondsLeft });
      }
    } catch (error) {
      console.log('error reading product', error);
    }
  };

  render() {
    return (
      
    
      <View style={{flex:1}}>
        
        <ScrollView>
        <View>
        
          <View style={styles.header}>
          <Icon style ={{position: 'absolute', top: 25,right: 0}}
            name="bell-circle"
            marginTop = {20}
            size = {50}
            color = 'white'
            onPress={() => this.props.navigation.navigate( 'Alerts' )}
          />
          
        </View>
        
          <Image style={styles.avatar} source={{ uri: this.state.currentUser.photoURL + "?height=500" }} />
          
          <View style={styles.body}>
            <Text style={styles.name}>{this.state.currentUser.displayName}</Text>
            <Text style={styles.status}>BloodChain PowerUser</Text>
            {/* <Text style={styles.info}>Your email : {this.state.currentUser.email}</Text> */}
            <View style={{ height: '23.5%' }}>
              <Button buttonStyle={styles.infoButton}
                title="Cum pot dona?"
                onPress={() => this.props.navigation.navigate( 'Info' )}
              />
              <Button buttonStyle={styles.infoButton}
                title="Vreau sa donez"
                onPress={() => this.props.navigation.navigate( 'Offers' )}
              />
              <Button buttonStyle={styles.logOutButton}
                onPress={() => {
                  firebase.auth().signOut().then(() => {
                    this.props.navigation.navigate("Register");
                    onSignOut();
                    AsyncStorage.removeItem('product');
                  });
                }}
                title="Deconectare"
              />
              
            </View>
            
            {this.state.product &&
            <View style={styles.countDown}>
              <CountDown
                until={this.state.date}
                timetoShow={('M', 'S')}
                onFinish={() => alert('finished')}
                onPress={() => alert('Mai ai inca catva timp pana poti sa donezi')}

                size={20}
              />
            </View>
            }
            {this.state.product &&
            <View style={{flex:1}}>
              <Text style={styles.offers} > Oferta activata de tine: </Text>
              <Text style={styles.offer}>{this.state.product.Title}</Text>
              <Text style={styles.offer}>{this.state.product.code}</Text>

            </View>
            }
            </View>          
          </View>
          
         </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  
  header: {
    // AlertButton: {
    //   alignSelf: 'center',
    //   marginTop: 130,
    //   position: 'absolute',
    // },
    
    // alignItems: 'flex',
    // flexDirection : 'column',
    backgroundColor: "#BA272A",
    height: 200
  },
  body: {
    alignItems: 'center',
    padding: 30,
    marginTop: 20
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: 130
  },
  name: {
    marginTop:10,
    fontSize: 30,
    color: "black",
    fontWeight: 'bold'
  },
  info: {
    fontSize: 16,
    color: "black",
    marginTop: 10 ,
    fontWeight:'800',
  },
  status: {
    fontSize: 20,
    color: "#BA272A",
    marginTop: 10,
    fontWeight:'bold',
  },
  description: {
    fontSize: 16,
    color: "#696969",
    marginTop: 10,
    textAlign: 'center'
  },
  infoButton: {
    
    marginTop: 20,
    height: 50,
    marginBottom: 0,
    width: 250,
    borderRadius: 30,
    backgroundColor: "#BA272A",
    
  },
  logOutButton: {
    // fontWeight: 'bold',
    marginTop: 20,
    height: 50,
    marginBottom: 15,
    width: 250,
    borderRadius: 30,
    backgroundColor: "#BA272A",
    
  },
  countDown: {
    marginTop: 95,
    height: 100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
    width: 300
  },
  offers: {
    fontSize: 18,
    color: "black",
    marginTop: -5,
    fontWeight:'bold',
  },
  offer: {
    fontSize: 18,
    color: "#BA272A",
    marginTop: 0,
    fontWeight:'bold',
    textAlign : 'center',
  },
});


HomeScreen.navigationOptions = {
  header: null,
};
