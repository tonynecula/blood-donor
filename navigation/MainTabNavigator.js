import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createAppContainer } from 'react-navigation';


import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import AlertScreen from '../screens/AlertScreen';
import OffersScreen from '../screens/OffersScreen';
import OfferDetailsScreen from '../screens/OfferDetailsScreen';
import QRScanScreen from '../screens/QRScanScreen';
import InfoScreen from '../screens/InfoScreen';
import NewsScreen from '../screens/NewsScreen';
import Post from '../screens/Post';
import OfferScreen from '../screens/OffersScreen';
const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Alerts: AlertScreen,
  },
  config
);
/* const RaffleStack = createStackNavigator(
  {
    Offer: OfferScreen,
    
  },
) 
 */
HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarOptions: {
    activeTintColor: '#BA272A',
    inactiveTintColor: 'gray',
  },
  tabBarIcon: ({tintColor,focused }) => (
    <TabBarIcon name='ios-home' focused ={focused} size={25} color={tintColor} />
  ),
  
};

HomeStack.path = '';


const OffersStack = createStackNavigator(
  {
    Offers: OffersScreen,
    OfferDetails: OfferDetailsScreen, 
    QRReader: QRScanScreen,
    Post: Post
  }, config
);

OffersStack.navigationOptions = {
  tabBarLabel: 'Offers',
  tabBarOptions: {
    activeTintColor: '#BA272A',
    inactiveTintColor: 'gray',
  },
  tabBarIcon: ({ tintColor, focused }) => {
    if (Platform.OS === 'ios')
      return <TabBarIcon name="ios-gift" focused={focused} color={tintColor} />
    return <TabBarIcon name={'md-gift'} focused={focused} />
  }    ,
  
};

OffersStack.path = '';

const InfoStack = createStackNavigator (
  {
      Info: InfoScreen,
  },config
  

);
InfoStack.navigationOptions = {
  tabBarLabel: 'Info',
  tabBarOptions: {
    activeTintColor: '#BA272A',
    inactiveTintColor: 'gray',
  },
  tabBarIcon: ({tintColor, focused}) => (
    <TabBarIcon name='ios-information-circle' focused ={focused} size={25} color={tintColor} />
  ),
  
};
InfoStack.path = '';

const NewsStack = createStackNavigator(
  {
   News: NewsScreen,
  },
  config
);

NewsStack.navigationOptions = {
  tabBarLabel: 'News',
  tabBarOptions: {
        activeTintColor: '#BA272A',
        inactiveTintColor: 'gray',
      },
      tabBarIcon: ({tintColor,focused }) => (
        <TabBarIcon name='ios-today' focused ={focused} size={25} color={tintColor} />
      ),
};

NewsStack.path = '';

// const SettingsStack = createStackNavigator(
//   {
//     Settings: SettingsScreen,
//   },
//   config
// );

// SettingsStack.navigationOptions = {
//   tabBarLabel: 'Settings', 
//   tabBarOptions: {
//     activeTintColor: '#BA272A',
//     inactiveTintColor: 'gray',
//   },
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
//   ),
// };

// SettingsStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  NewsStack,
  OffersStack,
  InfoStack,
  //RaffleStack
  //LinksStack,
  //SettingsStack,
});

tabNavigator.path = '';

export default tabNavigator;
