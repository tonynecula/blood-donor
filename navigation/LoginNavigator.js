import { createAppContainer, createSwitchNavigator } from "react-navigation";
import RegisterScreen from './../screens/RegisterScreen';

// import SignUp from "./screens/SignUp";
// import SignIn from "./screens/SignIn";

/* https://medium.com/the-react-native-log/building-an-authentication-flow-with-react-navigation-fb5de2203b5c */
/* https://medium.com/@juliofeferman/running-facebook-authentication-in-react-native-with-firebase-219a002588fa */

export default createAppContainer(
    createSwitchNavigator({
        SignUp: {
            screen: RegisterScreen,
            headerMode: 'none',
            navigationOptions: {
                //title: "Sign Up",
                headerVisible: false,
            }
        },
        //   SignIn: {
        //     screen: SignIn,
        //     navigationOptions: {
        //       title: "Sign In"
        //     }
        //   }
    }));