import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AppNavigator from './navigation/AppNavigator';
import LoginNavigator from './navigation/LoginNavigator';
import {isSignedIn, onSignIn} from './appState/appStore';
import firebase from './utils/firebase';
export default class App extends React.Component {
  constructor(props) {
    super(props);

    console.log('App.js constructor');
    this.state = {
      signedIn: false,
      checkedSignIn: false,
      isLoadingComplete: false
    };

    isSignedIn().then((res)=>{
      console.log('isSignedIn', res);
      if(res)
        this.setState({ signedIn: res, checkedSignIn: res });
    });

    //subscribe to auth changed event to update app if register screen will authenticate user
    firebase.auth().onAuthStateChanged(user => {
      //if current user is defined and app state is not authenticated -> update app state
      if (user != null && !this.state.isSignedIn) { 
        console.log('onAuthStateChanged:', user); //log current user info for further debug
        this.setState({ signedIn: true, checkedSignIn: true });
        isSignedIn().then((isSignedIn)=>{
          if(!isSignedIn)
          {
            console.log('signed in locally');
            onSignIn();
          }
        });
      }
    });
  }

  onNavigationChanged (){ }

  render() {
    console.log('App.js rendered');
    
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <View>
        <AppLoading
          startAsync={loadResourcesAsync}
          onError={handleLoadingError}
          onFinish={() => this.setState({ isLoadingComplete: true })}
        />
        </View>
      );
    }
    else if (this.state.signedIn)
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <AppNavigator onNavigationStateChange={this.onNavigationChanged()}/>
        </View>
      )
    else
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <LoginNavigator onNavigationStateChange={this.onNavigationChanged()}/>
        </View>
      )
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
      require('./assets/images/bloody.png'),
    ]),
    
  ]);
}

function handleLoadingError(error: Error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
