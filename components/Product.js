import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableHighlight
} from "react-native";
class Product extends Component {
    constructor(props) {
        super(props);
        console.log('Product details: ', props);
    }

    render() {
        return (
            <View style={styles.productStyle}>
                <TouchableHighlight style={{ height: 170 }} onPress={() => this.props.navigation.navigate('OfferDetails', { product: this.props.productObj })}>
                    <Image
                        style={{ flex: 1,width:'100%',height:120,
                        borderColor: 'black',
                        // fontWeight: 'bold',
                        borderWidth: 1.1,
                        borderRadius: 30,
                            }}
                        source={{ uri: this.props.imageUri }} />
                </TouchableHighlight>
                <Text style={{ fontSize: 25,fontWeight:'bold', color: '#BA272A',}}> {this.props.productObj.Title} </Text>
                <Text style={{ fontSize: 18,fontWeight:'bold',}}> {this.props.productObj.Description} </Text>
            </View>
        );
    }
}
export default Product;

const styles = StyleSheet.create({
    productStyle: {
        height: 250,
        marginBottom: 10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        
    }
});